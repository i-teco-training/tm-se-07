package ru.alekseev.tm.bootstrap;

import ru.alekseev.tm.api.*;
import ru.alekseev.tm.command.project.ProjectAddCommand;
import ru.alekseev.tm.command.project.ProjectDeleteCommand;
import ru.alekseev.tm.command.project.ProjectListCommand;
import ru.alekseev.tm.command.project.ProjectUpdateCommand;
import ru.alekseev.tm.command.system.AboutCommand;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.command.system.ExitCommand;
import ru.alekseev.tm.command.system.HelpCommand;
import ru.alekseev.tm.command.task.TaskAddCommand;
import ru.alekseev.tm.command.task.TaskDeleteCommand;
import ru.alekseev.tm.command.task.TaskListCommand;
import ru.alekseev.tm.command.task.TaskUpdateCommand;
import ru.alekseev.tm.command.user.*;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.repository.UserRepository;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;
import ru.alekseev.tm.service.UserService;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Bootstrap implements ServiceLocator {
    private IProjectRepository projectRepository = new ProjectRepository();
    private IProjectService projectService = new ProjectService(projectRepository);
    private ITaskRepository taskRepository = new TaskRepository();
    private ITaskService taskService = new TaskService(taskRepository);
    private IUserRepository userRepository = new UserRepository();
    private IUserService userService = new UserService(userRepository);

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    public void registry(AbstractCommand command) {
        String commandName = command.getName();
        commands.put(commandName, command);
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        initCommands();
        initUsers();

        while (true) {
            String commandName = reader.readLine();
            if (!commands.containsKey(commandName)) {
                commandName = "help";
            }

            if (commands.get(commandName).isSecure() && !this.userService.isAuthorized()) {
                System.out.println("LOGIN INTO PROJECT MANAGER");
                System.out.println();
                continue;
            }

            commands.get(commandName).execute();
            System.out.println();
        }
    }

    public void initCommands() {
        registry(new HelpCommand(this));
        registry(new UserSignupCommand(this));
        registry(new UserLoginCommand(this));
        registry(new UserLogoutCommand(this));
        registry(new UserLoadCurrentProfileCommand(this));
        registry(new UserEditCurrentProfileCommand(this));
        registry(new UserPasswordUpdateCommand(this));
        registry(new ProjectAddCommand(this));
        //registry(new ProjectClearCommand(this));
        registry(new ProjectDeleteCommand(this));
        registry(new ProjectListCommand(this));
        registry(new ProjectUpdateCommand(this));
        registry(new TaskAddCommand(this));
        //registry(new TaskClearCommand(this));
        registry(new TaskDeleteCommand(this));
        registry(new TaskListCommand(this));
        registry(new TaskUpdateCommand(this));
        registry(new AboutCommand(this));
        registry(new ExitCommand(this));
    }

    public void initUsers() {
        this.userService.addByLoginPasswordUserRole("user1", HashUtil.getMd5("www"), RoleType.admin);
        this.userService.addByLoginPasswordUserRole("user2", HashUtil.getMd5("www"), RoleType.user);
    }
}