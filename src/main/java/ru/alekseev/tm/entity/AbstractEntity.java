package ru.alekseev.tm.entity;

public abstract class AbstractEntity {
    public String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
