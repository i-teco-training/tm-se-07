package ru.alekseev.tm.repository;

import ru.alekseev.tm.api.IProjectRepository;
import ru.alekseev.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public final List<Project> findAllByUserId(final String userId) {
        final List<Project> allProjects = new ArrayList<>(map.values());
        final List<Project> filteredList = new ArrayList<>();
        for (Project project : allProjects) {
            if (project.getUserId().equals(userId))
                filteredList.add(project);
        }
        return filteredList;
    }

    @Override
    public final void deleteByUserId(final String userId) {
        final List<Project> projects = new ArrayList<>(map.values());
        for (Project project : projects) {
            if (project.getUserId().equals(userId))
                map.remove(project);
        }
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(final String userId, final String projectId, final String projectName) {
        Project requiredProject = map.get(projectId);
        if (userId.equals(requiredProject.getUserId())) {
            final Project project = new Project();
            project.setUserId(userId);
            project.setId(projectId);
            project.setName(projectName);
            map.put(project.getId(), project);
        }
    }

    @Override
    public final void deleteByUserIdAndProjectId(final String userId, final String projectId) {
        final Project projectForExistenceChecking = map.get(projectId);
        if (!userId.equals(projectForExistenceChecking.getUserId())) return;
        map.remove(projectId);
    }
}
