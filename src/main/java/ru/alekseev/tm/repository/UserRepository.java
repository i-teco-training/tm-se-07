package ru.alekseev.tm.repository;

import ru.alekseev.tm.api.IUserRepository;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public final User findOneByLoginAndPasswordHashcode(final String login, final String passwordHashcode) {
        final List<User> list = new ArrayList<>(map.values());
        for (User user : list) {
            if (login.equals(user.getLogin()) && passwordHashcode.equals(user.getPasswordHashcode()))
                return user;
        }
        return null;
    }

    @Override
    public final void deleteByLoginAndPassword(final String login, final String passwordHashcode) {
        final User userForExistenceChecking = findOneByLoginAndPasswordHashcode(login,passwordHashcode);
        if (userForExistenceChecking == null) return;
        map.remove(userForExistenceChecking.getId());
    }

    @Override
    public void addByLoginPasswordUserRole(String login, String passwordHashcode, RoleType roleType) {
        final User newUser = new User();
        newUser.setLogin(login);
        newUser.setPasswordHashcode(passwordHashcode);
        newUser.setRoleType(roleType);
        map.put(newUser.getId(), newUser);
    }
}
