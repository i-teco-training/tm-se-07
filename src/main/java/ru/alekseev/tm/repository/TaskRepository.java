package ru.alekseev.tm.repository;

import ru.alekseev.tm.api.ITaskRepository;
import ru.alekseev.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public final void clearByProjectId(final String projectId) {
        final List<Task> allTasks = new ArrayList<>(map.values());
        for (Task task : allTasks) {
            if (task.getProjectId().equals(projectId))
                map.remove(task);
        }
    }

    @Override
    public final List<Task> findAllByUserId(final String userId) {
        final List<Task> allTasks = new ArrayList<>(map.values());
        final List<Task> filteredList = new ArrayList<>(); //final?
        for (Task task : allTasks) {
            if (task.getUserId().equals(userId))
                filteredList.add(task);
        }
        return filteredList;
    }

    @Override
    public final void removeByUserIdAndTaskId(final String userId, final String taskId) {
        final Task taskForExistenceChecking = map.get(taskId);
        if (!userId.equals(taskForExistenceChecking.getUserId())) return;
        map.remove(taskId);
    }

    @Override
    public final void updateByNewData(final String userId, final String taskId, final String name) {
        final Task oldTask = map.get(taskId);
        final String projectId = oldTask.getProjectId();
        final Task task = new Task();
        task.setName(name);
        task.setId(taskId);
        task.setProjectId(projectId);
        map.put(task.getId(), task);
    }
}
