package ru.alekseev.tm.repository;

import ru.alekseev.tm.api.IRepository;
import ru.alekseev.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final Map<String, E> map = new HashMap<>();

    @Override
    public final List<E> findAll() {
       return new ArrayList<>(map.values());
    }

    @Override
    public final E findOne(final String id) {
        return map.get(id);
    }

    @Override
    public final void persist(final E e) {
        if (!map.containsKey(e.getId())) {
            map.put(e.getId(), e);
        }
    }

    @Override
    public final void merge(final E e) {
        map.put(e.getId(), e);
    }

    @Override
    public final void delete(final String id) {
        map.remove(id);
    }

    @Override
    public final void clear() {
        map.clear();
    }
}
