package ru.alekseev.tm.service;

import ru.alekseev.tm.api.IUserRepository;
import ru.alekseev.tm.api.IUserService;
import ru.alekseev.tm.entity.RoleType;
import ru.alekseev.tm.entity.User;

import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {
    private final IUserRepository userRepository;
    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }
    private User currentUser;

    @Override
    public final void delete(final String id) {
        this.userRepository.delete(id);
    }

    @Override
    public final User getCurrentUser() {
        return currentUser;
    }

    @Override
    public final void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    public final boolean isAuthorized() {
        return this.currentUser != null;
    }

    @Override
    public final void add(final User user) {
        this.userRepository.persist(user);
    }

    public final List<User> list() {
        return this.userRepository.findAll();
    }

    @Override
    public final User findOneByLoginAndPassword(final String login, final String passwordHashcode) {
        return this.userRepository.findOneByLoginAndPasswordHashcode(login,passwordHashcode);
    }

    @Override
    public final void update(final User user) {
        if (user == null) return;
        this.userRepository.merge(user);
    }

    @Override
    public final void deleteByLoginAndPassword(final String login, final String passwordHashcode) {
        this.userRepository.deleteByLoginAndPassword(login, passwordHashcode);
    }

    public final void clear() {
        this.userRepository.clear();
    }

    @Override
    public void clearByUserId(String id) {

    }

    @Override
    public void addByLoginPasswordUserRole(final String login, final String passwordHashcode, final RoleType roleType) {
        this.userRepository.addByLoginPasswordUserRole(login, passwordHashcode, roleType);
    }
}
