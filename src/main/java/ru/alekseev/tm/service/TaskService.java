package ru.alekseev.tm.service;

import ru.alekseev.tm.api.ITaskRepository;
import ru.alekseev.tm.api.ITaskService;
import ru.alekseev.tm.entity.Task;

import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public final List<Task> list() {
        return this.taskRepository.findAll();
    }

    @Override
    public final void update(final Task task) {
        this.taskRepository.merge(task);
    }

    @Override
    public final void delete(final String id) {
        this.taskRepository.delete(id);
    }

    @Override
    public final void clear() {
        this.taskRepository.clear();
    }

    @Override
    public final void add(final Task task) {
        this.taskRepository.persist(task);
    }

    @Override
    public final List<Task> findAllByUserId(final String userId) {
        return this.taskRepository.findAllByUserId(userId);
    }

    @Override
    public final void updateByNewData(final String userId, final String taskId, final String name) {
        if (taskId == null || taskId.isEmpty() || name == null || name.isEmpty()) return;
        this.taskRepository.updateByNewData(userId, taskId, name);
    }

    @Override
    public final void deleteByUserIdAndTaskId(final String userId, final String taskId) {
        this.taskRepository.removeByUserIdAndTaskId(userId, taskId);
    }

    @Override
    public final void clearByProjectId(final String projectId) {
        this.taskRepository.clearByProjectId(projectId);
    }
}
