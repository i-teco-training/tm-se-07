package ru.alekseev.tm.service;

import ru.alekseev.tm.api.IProjectRepository;
import ru.alekseev.tm.api.IProjectService;
import ru.alekseev.tm.entity.Project;

import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {
    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public final void update(final Project project) {

    }

    @Override
    public final void delete(final String id) {

    }

    @Override
    public final void clear() {

    }

    @Override
    public final void add(final Project project) {
        this.projectRepository.persist(project);
    }

    @Override
    public final List<Project> list() {
        return this.projectRepository.findAll();
    }

    public final List<Project> findAllByUserId(final String userId) {
        return this.projectRepository.findAllByUserId(userId);
    }

    @Override
    public final void updateByUserIdProjectIdProjectName(final String userId, final String projectId, final String projectName) {
        this.projectRepository.updateByUserIdProjectIdProjectName(userId, projectId, projectName);
    }

    @Override
    public final void deleteByUserIdAndProjectId(final String userId, final String projectId) {
        final Project projectForExistenceChecking = this.projectRepository.findOne(projectId);
        if (!userId.equals(projectForExistenceChecking.getUserId())) return;
        this.projectRepository.delete(projectId);
    }

    @Override
    public final void clearByUserId(final String userId) {
        this.projectRepository.deleteByUserId(userId);
    }
}
