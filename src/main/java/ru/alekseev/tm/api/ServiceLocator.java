package ru.alekseev.tm.api;

import ru.alekseev.tm.command.system.AbstractCommand;

import java.util.List;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    List<AbstractCommand> getCommands();
}
