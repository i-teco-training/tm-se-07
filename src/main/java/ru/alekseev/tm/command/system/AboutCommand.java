package ru.alekseev.tm.command.system;

import ru.alekseev.tm.api.ServiceLocator;

import com.jcabi.manifests.Manifests;

public class AboutCommand extends AbstractCommand {
    public AboutCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Get app version information";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Manifest Version is " + Manifests.read("Manifest-Version"));
        System.out.println("Сurrent build number is " + Manifests.read("AppVersion"));
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
