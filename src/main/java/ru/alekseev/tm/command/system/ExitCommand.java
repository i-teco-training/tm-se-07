package ru.alekseev.tm.command.system;

import ru.alekseev.tm.api.ServiceLocator;

public class ExitCommand extends AbstractCommand {
    public ExitCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Stop the Project Manager Application";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
