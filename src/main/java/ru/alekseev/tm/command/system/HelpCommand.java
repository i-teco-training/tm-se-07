package ru.alekseev.tm.command.system;

import ru.alekseev.tm.api.ServiceLocator;

import java.util.List;

public class HelpCommand extends AbstractCommand {
    public HelpCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() {
        List<AbstractCommand> allCommands = this.serviceLocator.getCommands();

        for (AbstractCommand abstractCommand : allCommands) {
            System.out.println(abstractCommand.getName() + " - " + abstractCommand.getDescription());
        }
    }

    @Override
    public boolean isSecure() {
        return false;
    }
}
