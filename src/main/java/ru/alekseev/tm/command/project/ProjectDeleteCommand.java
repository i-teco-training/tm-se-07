package ru.alekseev.tm.command.project;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectDeleteCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectDeleteCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "delete-project";
    }

    @Override
    public String getDescription() {
        return "Delete project by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectId = reader.readLine();
        String currentUserId = this.serviceLocator.getUserService().getCurrentUser().getId();
        this.serviceLocator.getProjectService().deleteByUserIdAndProjectId(currentUserId, projectId);
        this.serviceLocator.getTaskService().clearByProjectId(projectId);
        System.out.println("[OK. PROJECT AND IT'S TASKS DELETED]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
