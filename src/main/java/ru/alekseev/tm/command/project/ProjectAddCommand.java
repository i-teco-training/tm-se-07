package ru.alekseev.tm.command.project;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.Project;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ProjectAddCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ProjectAddCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "add-project";
    }

    @Override
    public String getDescription() {
        return "Add new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        String projectName = reader.readLine();
        String currentUserId = this.serviceLocator.getUserService().getCurrentUser().getId();
        Project project = new Project();
        project.setName(projectName);
        project.setUserId(currentUserId);
        this.serviceLocator.getProjectService().add(project);
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
