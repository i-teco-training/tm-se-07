package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;
import ru.alekseev.tm.utility.HashUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class UserLoadCurrentProfileCommand extends AbstractCommand {
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public UserLoadCurrentProfileCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "load-profile";
    }

    @Override
    public String getDescription() {
        return "Load your Project Manager profile";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOADING YOUR PROFILE]");
        User currentUser = this.serviceLocator.getUserService().getCurrentUser();
        System.out.println("Your login: " + currentUser.getLogin());
        System.out.println("Your id: " + currentUser.getId());
        if (currentUser.getRoleType() != null) {
            System.out.println("Your account type: " + currentUser.getRoleType().toString());
        }
        System.out.println("[OK]");
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
