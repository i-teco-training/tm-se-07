package ru.alekseev.tm.command.user;

import ru.alekseev.tm.api.ServiceLocator;
import ru.alekseev.tm.command.system.AbstractCommand;
import ru.alekseev.tm.entity.User;

import java.util.List;

public class UserListCommand extends AbstractCommand {
    public UserListCommand(ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public String getName() {
        return "show-users";
    }

    @Override
    public String getDescription() {
        return "Show list of all users";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LIST OF ALL USERS]");
        List<User> allUsers = this.serviceLocator.getUserService().list();
        if (allUsers.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
        }
        for (int i = 0; i < allUsers.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") login:" + allUsers.get(i).getLogin() + ", userId: " + allUsers.get(i).getId());
        }
    }

    @Override
    public boolean isSecure() {
        return true;
    }
}
